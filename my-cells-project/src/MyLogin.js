import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './MyLogin-styles.js';
import '@bbva-web-components/bbva-form-text/bbva-form-text.js';
import '@bbva-web-components/bbva-form-password/bbva-form-password.js';
import '@bbva-web-components/bbva-button-default/bbva-button-default.js';
import '@bbva-web-components/bbva-progress-content/bbva-progress-content.js';
/**
This component ...

Example:

```html
<my-cells-project></my-cells-project>
```

##styling-doc

@customElement my-cells-project
@polymer
@LitElement
@demo demo/index.html
*/
export class MyLogin extends LitElement {
  static get is() {
    return 'my-cells-project';
  }

  // Declare properties
  static get properties() {
    return {
      name: { 
        type: String, 
      },
      userInputLabel: {
        type: String,
        attribute: 'user-input-label'
      },
      userInputValue: {
        type: String,
        attribute: 'user-input-value'
      },
      passwordInputLabel: {
        type: String,
        attribute: 'password-input-label'
      },
      passwordInputValue: {
        type: String,
        attribute: 'password-input-value'
      },
      buttonText: {
        type: String,
        attribute: 'button-text'
      },
      loading: {
        type: Boolean
      }
    };
  }

    // Initialize properties
  constructor() {
    super();
    //this.name = 'Cells';
    this.userInputLabel = 'Username';
    this.passwordInputLabel = 'Password';
    this.buttonText = 'Submit';
    this.loading = false;
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('my-cells-project-shared-styles')
    ]
  }


  _submit(ev) {
    ev.preventDefault();
    this.loading = true;
    this.dispatchEvent(new CustomEvent('my-cells-project-submit', {
      bubbles: true,
      composed: true,
      detail: {
        username: this.userInputValue,
        password: this.passwordInputValue
      }
    }));
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <!-- <p>Welcome to ${this.name}</p> --> 
      <form ?hidden="${this.loading}">
        <bbva-form-text
          label="${this.userInputLabel}"
          .value="${this.userInputValue}"
          @input="${(e) => this.userInputValue = e.target.value}">
        </bbva-form-text>
        <bbva-form-password
          label="${this.passwordInputLabel}"
          .value="${this.passwordInputValue}"
          @input="${(e) => this.passwordInputValue = e.target.value}">
        </bbva-form-password>
        <bbva-button-default
          ?disabled="${!this.userInputValue || !this.passwordInputValue}"
          @click="${this._submit}">
          ${this.buttonText}
        </bbva-button-default>
      </form>
      <bbva-progress-content ?hidden="${!this.loading}"></bbva-progress-content>
    `;
  }
}
