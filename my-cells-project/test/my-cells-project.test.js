import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../my-cells-project.js';

suite('<my-cells-project>', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<my-cells-project></my-cells-project>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});





